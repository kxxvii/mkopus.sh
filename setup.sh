#!/bin/bash

# I'd just place 'mkopus' wherever you want it. I thought
# that this might be a fun project for the future. So it's
# and implied "TODO-file"

# Yeah, not pretty, but it works. Might add 'getopts'
# if it makes sense. See license at 'mkopus.sh'


paths=("$HOME/bin" "/usr/local/bin")


if [[ "$1" == "-h" ]]; then
    printf "%b" "\n" \
                "  ./setup.sh [-h|-u]\n\n" \
                "  -h this help\n"         \
                "  -u unlink\n\n"
    exit 0
fi


for p in ${paths[*]}
do
    if [[ -d "$p" ]]; then

        # Reused
        mklink="$p/mkopus"

        if [[ "$1" == '-u' ]] && [[ -a "$mklink" ]]; then

            if [[ -L "$mklink" ]]; then
                
                if [[ ! -w "$p" ]]; then
                    sudo unlink "$mklink"
                else
                    unlink "$mklink"
                fi
                printf "%b" "  Unlinked $mklink\n" 
                exit 0
            else
                printf "%b" "  Symlink not found\n"
                exit 1
            fi

        elif [[ "$1" != '-u' ]]; then
            
            if [[ -a "$mklink" ]]; then
                printf "%b" "  There's already a symlink at $mklink\n"
                exit 1
            else
                printf "%b" "\n  FOUND: $p\n\n  Install at '$mklink'? [y]\n"
                read -r -p "  > "
            fi

            if [[ "$REPLY" == 'y' ]]; then
                
                if [[ ! -w "$p" ]]; then
                    sudo ln -s "$PWD/mkopus.sh" "$mklink"
                else
                    ln -s "$PWD/mkopus.sh" "$mklink"
                fi

                printf "%b" "  Uninstall with 'unlink $mklink' or '-u'\n"
                exit 0
            fi

        fi

    fi
done



